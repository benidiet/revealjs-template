# Outputs the reading time

# Read this in “about 4 minutes”
# Put into your _plugins dir in your Jekyll site
# Usage: Read this in about {{ page.content | reading_time }}
require "jekyll"
require 'uri'

class DatahubLink < Liquid::Tag
    def initialize(tagName, content, tokens)
        super
        @content = content
    end

    def render(context)       
        @uri = URI.parse(@content.strip)       
        @filename=@uri.path.split('blob').last      
        @branch=@uri.path.split('blob').last.split('/')[1]
        @filename.slice! @branch + "/"
        @repo = URI::HTTPS.build(host: @uri.host, path: @uri.path.split('blob').first.chop)
        @repo_project = @repo.to_s.split('/').last
        @datahub_url = Jekyll.configuration({})['jupyterhub_url']

        # Example of generated URL: 
        # https://datahub.cs.hm.edu/hub/user-redirect/git-pull?repo=https%3A%2F%2Fgithub.com%2FBZoennchen%2Fct-ws2022-students&urlpath=lab%2Ftree%2Fct-ws2022-students%2Fstudents%2Flecture02%2Fexpressions.ipynb&branch=main

        # 1) datahub_url: https://datahub.cs.hm.edu/hub/user-redirect/git-pull?repo=
        nbgitpullerurl  = @datahub_url
        nbgitpullerurl += "hub/user-redirect/git-pull?repo="

        # 2) repo_dir: https%3A%2F%2Fgithub.com%2FBZoennchen%2Fct-ws2022-students
        nbgitpullerurl += ERB::Util.url_encode(@repo.to_s)

        # 3) target_dir: &urlpath=lab%2Ftree%2Fct-ws2022-students%2Fstudents%2Flecture02%2Fexpressions.ipynb
        nbgitpullerurl += "&urlpath=" + ERB::Util.url_encode("lab/tree/" + @repo_project + @filename)

        # 4) branch_name: &branch=main
        nbgitpullerurl += "?branch=" + @branch
        
        %Q{<a href="#{ nbgitpullerurl }" target="_blank"> #{ @filename.split('/').last }&nbsp; <i class="fa fa-book"></i></a>}
    end

    Liquid::Template.register_tag "datahublink", self
end

  