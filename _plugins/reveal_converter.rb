#class Kramdown::Parser::ERBKramdown < Kramdown::Parser::Kramdown
#
#   def initialize(source, options)
#     super
#     @span_parsers.unshift(:erb_tags)
#   end
#
#   ERB_TAGS_START = /<%.*?%>/
#
#   def parse_erb_tags
#     @src.pos += @src.matched_size
#     @tree.children << Element.new(:raw, @src.matched)
#   end
#   define_parser(:erb_tags, ERB_TAGS_START, '<%')
#
#end


class Kramdown::Converter::Html
  # You can override any function from this page:
  # https://kramdown.gettalong.org/rdoc/Kramdown/Converter/Html.html
  
  # Inserts sections in case of horizontal rulers (-----)
  def convert_hr(el, indent)
    "#{' ' * indent}</section>\n\n#{' ' * indent}<section#{html_attributes(el.attr)}>"
  end
  
  # 
  def convert_codeblock(el, indent)
    attr = el.attr.dup
    lang = extract_code_language!(attr)
    hl_opts = {}
    highlighted_code = highlight_code(el.value, el.options[:lang] || lang, :block, hl_opts)

    result = escape_html(el.value)
    result.chomp!
    if el.attr['class'].to_s =~ /\bshow-whitespaces\b/
      result.gsub!(/(?:(^[ \t]+)|([ \t]+$)|([ \t]+))/) do |m|
        suffix = ($1 ? '-l' : ($2 ? '-r' : ''))
        m.scan(/./).map do |c|
          case c
          when "\t" then "<span class=\"ws-tab#{suffix}\">\t</span>"
          when " " then "<span class=\"ws-space#{suffix}\">&#8901;</span>"
          end
        end.join('')
      end
    end

    # Add live-coding='' per default
    # if not attr.has_key? 'live-coding'
    #   attr[:'live-coding'] = ''
    # end

    code_attr = {}
    code_attr['class'] = "#{lang}" if lang
    "#{' ' * indent}<pre#{html_attributes(attr)}>" \
      "<code#{html_attributes(code_attr)} data-trim contenteditable='true'>#{result}\n</code></pre>\n"
  end

  # Setting the default alignment for paragraphs to 'left'
  def convert_p(el, indent)
    # Image?
    if el.children.size == 1 && (el.children.first.type == :img || (el.children.first.type == :html_element && el.children.first.value == "img"))
      #convert(el.children.first, indent)
      #convert(el.attr, inner(el, indent), indent)
      #puts el.children.first.inspect
      #puts el.inspect
      #class << el.children.first.attr
      #  attr_accessor :style
      #end
      #el.children.first.attr.style = el.attr.style
      #puts el.children.first.inspect
      #convert_img(el.children.first, indent)
      "<img #{html_attributes(el.attr)} #{html_attributes(el.children.first.attr)} />\n"
    # Transparent?
    elsif el.options[:transparent]
      inner(el, indent)
    # seems to be a normal paragraph
    else
      if not el.attr.has_key? 'align'
        el.attr[:'align'] = 'left'
      end
      format_as_block_html("p", el.attr, inner(el, indent), indent)
    end
  end

end

class Jekyll::Converters::Markdown::MarkdownRevealConverter
  def initialize(config)
    require 'kramdown'
    @config = config
  rescue LoadError
    STDERR.puts 'You are missing a library required for Markdown. Please run:'
    STDERR.puts '  $ [sudo] gem install kramdown'
    raise FatalException.new("Missing dependency: kramdown")
  end

  def convert(content)
    Kramdown::Document.new(content, 
      #:input => 'ERBKramdown'
    ).to_html
  end
end
