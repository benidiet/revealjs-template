# (The MIT License)
#
# Copyright (c) 2014-2019 Yegor Bugayenko
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the 'Software'), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED 'AS IS', WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

require 'digest'
require 'fileutils'

module Jekyll
  class PythonBlock < Liquid::Block
    def initialize(tag_name, markup, tokens)
      super
      @html = markup
    end

    def render(context)
      site = context.registers[:site]
      name = Digest::MD5.hexdigest(super)

      if !File.exists?(File.join(site.dest, "gfx/#{name}.svg"))
        pythonscript = File.join(site.source, "tmp/#{name}.py")
        svg = File.join(site.source, "gfx/#{name}.svg")

        if File.exists?(svg)
          # puts "File #{svg} already exists (#{File.size(svg)} bytes)"
        else
          FileUtils.mkdir_p(File.dirname(pythonscript))
          File.open(pythonscript, 'w') { |f|
            f.write(super)
            f.write("plt.savefig('#{svg}', bbox_inches = 'tight', pad_inches = 0, transparent=True)\n")
          }
          #system("eval \"$(conda shell.bash hook)\" && conda activate ml1 && python #{pythonscript}") or raise "Python error: #{super}"
          system("python3 #{pythonscript}") or raise "Python error: #{super}"
          site.static_files << Jekyll::StaticFile.new(site, site.source, 'gfx', "#{name}.svg")
          puts "File #{svg} created (#{File.size(svg)} bytes)"
        end
      end

      source = "<img src='#{site.baseurl}/gfx/#{name}.svg' #{@html} type='image/svg+xml' #{@html}>"

    end
  end
end

Liquid::Template.register_tag('pythonscript', Jekyll::PythonBlock)

