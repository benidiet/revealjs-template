#!/usr/bin/env python
"""
Source: https://github.com/jgm/pandocfilters/blob/master/examples/plantuml.py

Pandoc filter to process code blocks with class "plantuml" into
plant-generated images.
Needs `plantuml.jar` from http://plantuml.com/.
"""

import os
import sys
from subprocess import call

from pandocfilters import toJSONFilter, Para, Image, get_filename4code, get_caption, get_extension


def plantuml(key, value, format, _):
    if key == 'CodeBlock':
        [[ident, classes, keyvals], code] = value

        if "plantuml" in classes:
            caption, typef, keyvals = get_caption(keyvals)

            filename = get_filename4code("plantuml", code)
            filetype = get_extension(format, "svg", html="svg", latex="eps")

            src = filename + '.uml'
            dest = filename + '.' + filetype

            if not code.startswith("@start"):
                code = "@startuml\n" + code + "\n@enduml\n" 
                
            if not os.path.isfile(dest):
                with open(src, "w") as f:
                    f.write(code)

                call(["java", "-jar", "templates/revealjs-template/_bin/plantuml.jar", "-t"+filetype, src])
                sys.stderr.write('Created image ' + dest + '\n')

            keyvals.append(["data-puml-code", code])
            return Para([Image([ident, [], keyvals], caption, ['../../' + dest, typef])])

if __name__ == "__main__":
    toJSONFilter(plantuml)