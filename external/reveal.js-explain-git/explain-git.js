import { HistoryView } from "./historyview.js";
import { ControlBox } from "./controlbox.js";

const Plugin = () => {

    let deck;

    function openExplainGit () {
        var explainGitElements = document.querySelectorAll('div.explain-git');

        explainGitElements.forEach(function(element) {
            var originView, historyView, controlBox;

            var revealConfig = deck.getConfig();

            // Parse configuration
            var jsonConfigString = element.innerHTML.replace(/<\!--/g, "")
                                                    .replace(/-->/g, "");
            var configuration = JSON.parse(jsonConfigString);
            var explainGitContainer = d3.select(element);

            // Compute and set container's width
            var containerWidth = (revealConfig.width * parseInt(configuration.width, 10) / 100);
            var containerHeight = (revealConfig.height * parseInt(configuration.height, 10) / 100);
            element.style.width = String(containerWidth) + 'px';
            element.style.height = String(containerHeight) + 'px';

            configuration.width = containerWidth - 400;
            configuration.height = containerHeight;
            configuration.baseLine = 0.6;

            if (configuration.originData) {
                configuration.height = configuration.height  * 0.5;

                originView = new HistoryView({
                    name: name + '-Origin',
                    width: configuration.width,
                    height: configuration.height,
                    commitRadius: 20,
                    remoteName: 'origin',
                    commitData: configuration.originData,
                    baseLine: configuration.baseLine
                });
            }
            historyView = new HistoryView(configuration);

            // Add ControlBox
            controlBox = new ControlBox({
                historyView: historyView,
                originView: originView,
                initialMessage: configuration.initialMessage
            });
            controlBox.render(explainGitContainer);

            // Add History View Container
            var div = document.createElement('div');
            div.classList.add('history-view');
            div.setAttribute('width', configuration.width);
            element.appendChild(div);           
            var historyViewContainer = explainGitContainer.select('.history-view');

            if (originView) {
                originView.render(historyViewContainer);
            }
            historyView.render(historyViewContainer);

        });
    }

    return {
        id: 'ExplainGit',

        init: function(reveal) {
            deck = reveal;
            openExplainGit();
        },

        open: openExplainGit
    };
};

export default Plugin;