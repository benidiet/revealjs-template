
const Plugin = () => {

    var deck;

    function generaterMultipleChoiceLink(json) {
        var desiredLink = "https://www.vote.ac/start/index.php?"
        desiredLink += "mc=";
        desiredLink += json["multiselect"] == true ? "1" : "0" || "0";
        desiredLink += "&p=";
        desiredLink += json["show_percentage"] == true ? "1" : "0" || "1";
        desiredLink += "&eid=";
        desiredLink += json["one_vote_per_device"] == true ? "1" : "0" || "1";
        desiredLink += "&a=" + String(json["answers"].length);
        desiredLink += "&ft=" + encodeURIComponent(json["question"]) + "qqQqqQ";

        var index = 0
        for (var answer of json["answers"]) {
            desiredLink += encodeURIComponent(String.fromCharCode("a".charCodeAt() + index) + ") " + answer) + "qqQ";
            index += 1;
        }
        desiredLink += "&m=1";

        return desiredLink;
    }

    function generateYesNoLink(json) {
        //https://www.vote.ac/start/index.php?mc=0&p=0&eid=0&a=1&ft=Ja%20oder%20nein?&m=2

        var desiredLink = "https://www.vote.ac/start/index.php?"
        desiredLink += "mc=";
        desiredLink += json["multiselect"] == true ? "1" : "0" || "0";
        desiredLink += "&p=";
        desiredLink += json["show_percentage"] == true ? "1" : "0" || "1";
        desiredLink += "&eid=";
        desiredLink += json["one_vote_per_device"] == true ? "1" : "0" || "1";
        desiredLink += "&a=1";
        desiredLink += "&ft=" + encodeURIComponent(json["question"]) + "qqQqqQ";
        desiredLink += "&m=2";
        return desiredLink;
    }

    function generateWordCloudLink(json) {
        //https://www.vote.ac/start/index.php?fs=0&of=1&w=60&ft=Das%20würde%20ich%20gerne%20entwickeln...&m=3

        var desiredLink = "https://www.vote.ac/start/index.php?"
        desiredLink += "fs=" + json["continue"] == true ? "1" : "0" || "0";
        desiredLink += "&of=" + json["direct_show"] == true ? "1" : "0" || "0";;
        desiredLink += "&w=";
        desiredLink += json["waiting_time"] || 60;
        desiredLink += "&ft=" + encodeURIComponent(json["question"]) + "qqQqqQ";
        desiredLink += "&m=3";
        return desiredLink;
    }
    function initialize(Reveal) {

        var config = Reveal.getConfig().eduvote || {};
		var eduvotes = document.getElementsByClassName('eduvote');
        for (let i = 0; i < eduvotes.length; i++) {

            var linkToVote = document.createElement("a");
            linkToVote.innerHTML = "Umfrage starten <br>(Link für Dozent:in)"
            var raw_string = eduvotes[i].innerHTML;
            raw_string = raw_string.replace("<!--", "");
            raw_string = raw_string.replace("-->", "");
            raw_string = raw_string.replace(/\r?\n|\r/g, '')
            raw_string = raw_string.replace(" ", "")

            var json = JSON.parse(raw_string);
            var link = ""
            if (json["type"] == "multiple-choice") {
                link = generaterMultipleChoiceLink(json);
            } else if (json["type"] == "yes-no") {
                link = generateYesNoLink(json);
            } else if (json["type"] == "word-cloud") {
                link = generateWordCloudLink(json);
            }
            linkToVote.setAttribute('href', link);
            linkToVote.setAttribute('target', '_blank');
            eduvotes[i].appendChild(linkToVote);
        }
    }

    return {
        id: 'RevealEduVote',

        init: function( reveal ) {
            deck = reveal;
            initialize(deck);
        },

        open: initialize
    };
};

export default Plugin;
