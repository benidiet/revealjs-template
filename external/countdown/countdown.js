
const Plugin = () => {

    class MyTimer {

        constructor(value, div) {
            this._value = value;
            this._current_value = value;
            this._is_running = false;
            this._html_element = div;
            this.Initialize()
        }

        Initialize() {
            var self = this;
            var startButton = document.createElement("BUTTON");
            startButton.innerHTML = "Start";
            // Configure run-button
            startButton.style.float = "center";
            startButton.addEventListener("click", function () {
                if (self._is_running) {
                    self.Stop();
                } else {
                    self.Start();
                }
            });
            this._html_element.appendChild(startButton);
            this._start_button=startButton;

            var resetButton = document.createElement("BUTTON");
            resetButton.innerHTML = "Reset";
            // Configure run-button
            resetButton.style.float = "center";
            resetButton.addEventListener("click", function () {
                self.Stop();
                self._current_value = self._value;
                self.updateHtml();
            });
            this._html_element.appendChild(resetButton);

            var addMinuteButton = document.createElement("BUTTON");
            addMinuteButton.innerHTML = "+1:00";
            // Configure run-button
            addMinuteButton.style.float = "center";
            addMinuteButton.addEventListener("click", function () {
                self._current_value += 60;
                self.updateHtml();
            });
            this._html_element.appendChild(addMinuteButton);

            this._time_txt = document.createElement("h1");
            this._time_txt.innerHTML = "00:00";
            this._html_element.appendChild(this._time_txt);
            this.updateHtml();
        }

        Start() {
            if (this._current_value == 0) {
                return;
            }

            this._is_running = true;
            if (this._interval) {
                clearInterval(this._interval);
            }

            var self = this;
            this._interval = setInterval(function() {
                self._current_value = self._current_value - 1;
                if (self._current_value == 0) {
                    self.Stop();
                }
                self.updateHtml();
            }, 1000);
            
            this.updateHtml();
        }

        Stop() {
            this._is_running = false;
            if (this._interval) {
                clearInterval(this._interval);
            }
            this.updateHtml();
        }

        updateHtml() {
            var minutes = Math.floor(this._current_value / 60);
            var seconds = Math.floor(this._current_value % 60);
            this._time_txt.innerHTML = ("0" + minutes).slice(-2) + ":" + ("0" + seconds).slice(-2);
            if (this._is_running) {
                this._start_button.innerHTML = "Stop";
            } else {
                this._start_button.innerHTML = "Start";
            }
        }

        Reset() {
            this.Stop();
        }
    };

    var deck;

    function initialize(Reveal) {
		var countdowns = document.getElementsByClassName('countdown');
        for (let i = 0; i < countdowns.length; i++) {
            var timer = new MyTimer(countdowns[i].dataset.timeInSeconds, countdowns[i]);
        }
    }

    return {
        id: 'RevealCountdown',

        init: function( reveal ) {
            deck = reveal;
            initialize(deck);
        },

        open: initialize
    };
};

export default Plugin;
