
const Plugin = () => {

    var deck;

    function initialize(Reveal) {

        var config = Reveal.getConfig().bugreport || {};

        var doShowBugReportButton = config.doShowBugReportButton == undefined ? true : config.doShowBugReportButton;

        if ( doShowBugReportButton ) {
            var button = document.createElement( 'div' );
            button.className = "bugreport-button";
            button.id = "create-bugreport";
            button.style.visibility = "visible";
            button.style.position = "absolute";
            button.style.zIndex = 30;
            button.style.fontSize = "24px";
    
            button.style.left = config.button.left || "150px";
            button.style.bottom = "30px";
            button.style.top = "auto";
            button.style.right = "auto";
            button.innerHTML = '<a><i class="fas fa-bug"></i></a>'
            button.addEventListener("click", function () {
                createBugReport(Reveal);
                return false;
            });
            document.querySelector(".reveal").appendChild( button );
        }
    }

    function createBugReport(Reveal) {
        var slideNumber = Reveal.getIndices()
        var h = parseInt(slideNumber.h) + 1
        var v = parseInt(slideNumber.v) + 1
        var config = Reveal.getConfig().bugreport || {email: "bitte in _config.yml konfigurieren", title: "bitte in _config.yml konfigurieren"}

        var receiver = config.email
        var subject = encodeURI("[" + config.lecture + "]" + " Fehler in Folie ") + String(h) + "." + String(v)
        var body = encodeURI("URL: ") + window.location.href + encodeURI("\nFehler: ")
        var mailto = "mailto:" + receiver + "?subject=" + subject + "&body=" + body

        window.location =  mailto
    }

    return {
        id: 'RevealBugReport',

        init: function( reveal ) {
            deck = reveal;
            initialize(deck);
        },

        open: initialize
    };
};

export default Plugin;
